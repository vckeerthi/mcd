package activity3mcd;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class mcdonald {
	WebDriver driver;

	@BeforeEach
	void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", 
				"/Users/sm/Desktop/chromedriver");
	    driver = new ChromeDriver();
		
		driver.get("https://www.mcdonalds.com/ca/en-ca.html");

		driver.findElement(By.xpath("//*[@id=\"maincontent\"]/div[2]/div[2]/div/div[1]/div/a")).click();
		
		
	}

	@AfterEach
	void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.close();
		
	}
	

	@Test
	public void testcase1() throws InterruptedException  {
		
		WebElement heading = driver.findElement(By.xpath("//*[@id=\"maincontent\"]/div[1]/div[3]/div[1]/div/div[2]/div/div/div/div/div/div[1]/div[1]/h2"));
		String actualheading = heading.getText();
		assertEquals("Subscribe to My McD’s®",actualheading);
		
	}
	@Test
	public void testcase2() throws InterruptedException{
		WebElement nameBox = driver.findElement(By.id("firstname2"));
		nameBox.sendKeys("chandra keerthi");
		String name = nameBox.getText();
		WebElement email = driver.findElement(By.id("email2"));
		email.sendKeys("chandrakeerthi246@gmail.com");
		WebElement postalCode = driver.findElement(By.id("postalcode2"));
		postalCode.sendKeys(" m1t ");
		WebElement showButton = driver.findElement(
				By.id("g-recaptcha-btn-2"));
		showButton.click();
	}
	@Test
	public void testcase3() throws InterruptedException{
		WebElement nameBox = driver.findElement(By.id("firstname2"));
		nameBox.sendKeys("");
		String name = nameBox.getText();
		WebElement email = driver.findElement(By.id("email2"));
		email.sendKeys("abc@.com");
		WebElement postalCode = driver.findElement(By.id("postalcode2"));
		postalCode.sendKeys(" t ");
		WebElement showButton = driver.findElement(
				By.id("g-recaptcha-btn-2"));
		showButton.click();

//		WebElement error1 = driver.findElement(By.className("rc-imageselect-incorrect-response"));
//		String actualerror = error1.getText();
//		assertEquals("Please try again",actualerror);
//		//*[@id="rc-imageselect"]/div[2]/div[3]
//		
		
	}

}
